﻿using EmergenciaMedica.Dominio.Entities;

namespace EmergenciaMedica.Dominio.Interfaces.Repository
{
    public interface IUnidadeSaudeRepository : IRepositoryBase<UnidadeSaude>
    {
    }
}
