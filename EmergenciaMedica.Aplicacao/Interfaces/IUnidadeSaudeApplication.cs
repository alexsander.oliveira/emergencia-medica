﻿using EmergenciaMedica.Dominio.Entities;
using System.Collections.Generic;

namespace EmergenciaMedica.Aplicacao.Interfaces
{
    public interface IUnidadeSaudeApplication : IApplicationServiceBase<UnidadeSaude>
    {
        List<UnidadeSaude> CarregarCsvDocumentoESalvarNaBase();
    }
}
