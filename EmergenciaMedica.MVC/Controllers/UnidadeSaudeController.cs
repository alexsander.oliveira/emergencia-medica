﻿using EmergenciaMedica.Aplicacao.Interfaces;
using EmergenciaMedica.Dominio.Entities;
using EmergenciaMedica.MVC.Models;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using EmergenciaMedica.MVC.AutoMapper;

namespace EmergenciaMedica.MVC.Controllers
{
    public class UnidadeSaudeController : Controller
    {
        private readonly IUnidadeSaudeApplication _unidadeSaudeApplication;
        private readonly IMapper _mapper;

        public UnidadeSaudeController(IUnidadeSaudeApplication unidadeSaudeApplication)
        {
            _unidadeSaudeApplication = unidadeSaudeApplication;
            _mapper = AutoMapperConfig.Mapper;
        }


        // GET: UnidadeSaude
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult BuscarUnidadesSaude()
        {
            var listaUnidadeSaudeVM = _mapper.Map<IEnumerable<UnidadeSaude>, IEnumerable<UnidadeSaudeVM>>(_unidadeSaudeApplication.GetAll());
            if (listaUnidadeSaudeVM != null)
            {
                return Json(new { dados = listaUnidadeSaudeVM, sucesso = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { erro = "Nenhum dado encontrado", sucesso = false }, JsonRequestBehavior.AllowGet);
            }
            
        }

        public JsonResult CarregarCSVDocumento()
        {
            _unidadeSaudeApplication.CarregarCsvDocumentoESalvarNaBase();
            return Json(new { erro = "", sucesso = false }, JsonRequestBehavior.AllowGet);
        }
        
    }
}
