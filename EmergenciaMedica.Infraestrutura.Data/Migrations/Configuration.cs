namespace EmergenciaMedica.Infraestrutura.Data.Migrations
{
    using Aplicacao.Implementation;
    using Aplicacao.Interfaces;
    using Dominio.Entities;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.IO;

    internal sealed class Configuration : DbMigrationsConfiguration<EmergenciaMedica.Infraestrutura.Context.EmergenciaMedicaContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(EmergenciaMedica.Infraestrutura.Context.EmergenciaMedicaContext context)
        {
            
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }

    }
}
