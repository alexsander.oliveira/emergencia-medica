﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EmergenciaMedica.MVC.Models
{
    public class UnidadeSaudeVM
    {
        #region Propriedades
            [Key]
            public int Id { get; set; }
            public string TipoInstituicao { get; set; }
            public string TipoEmergencia { get; set; }
            public string Nome { get; set; }
            public string Endereco { get; set; }
            public string Telefone { get; set; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }
            public string LinkPt { get; set; }
            public string LinkEs { get; set; }
            public string EspecialidadesPt { get; set; }
            public string EspecialidadesEn { get; set; }
            public string EspecialidadesEs { get; set; }
        #endregion
    }
}