﻿using EmergenciaMedica.Aplicacao.Interfaces;
using EmergenciaMedica.Dominio.Entities;
using EmergenciaMedica.Dominio.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace EmergenciaMedica.Aplicacao.Implementation
{
    public class UnidadeSaudeApplication : ApplicationServiceBase<UnidadeSaude>, IUnidadeSaudeApplication
    {
        private readonly IUnidadeSaudeService _unidadeSaudeService;

        public UnidadeSaudeApplication(IUnidadeSaudeService unidadeSaudeService)
            : base(unidadeSaudeService)
        {
            _unidadeSaudeService = unidadeSaudeService;
        }
        
        public List<UnidadeSaude> CarregarCsvDocumentoESalvarNaBase()
        {
            string text = File.ReadAllText(@"c:\\Projetos\\doctorpoa.csv", Encoding.GetEncoding("ISO-8859-1"));

            List<UnidadeSaude> unidadesSaude = new List<UnidadeSaude>();
            var dados = text.Split('\n');
            var colunas = dados[0].Split(';');
            for (int i = 1; i < dados.Length; i++)
            {
                var modelo = new UnidadeSaude();
                var registros = dados[i].Split(';');
                for (int r = 0; r < registros.Length; r++)
                {
                    var propriedade = EncontraPropriedadePorNome(colunas[r]);
                    if (propriedade != null)
                    {
                        propriedade.SetValue(modelo, Convert.ChangeType(registros[r], propriedade.PropertyType), null);
                    }
                }
                unidadesSaude.Add(modelo);
            }

            foreach(var unidade in unidadesSaude)
            {
                _unidadeSaudeService.Add(unidade);
            }

            return null;
        }

        public static PropertyInfo EncontraPropriedadePorNome(string nome)
        {

            var propriedades = typeof(UnidadeSaude).GetProperties();
            var propriedade = propriedades.FirstOrDefault(p => p.Name.ToUpper().Contains(nome.ToUpper().Replace(" ", "")));
            return propriedade;
        }
    }
}
