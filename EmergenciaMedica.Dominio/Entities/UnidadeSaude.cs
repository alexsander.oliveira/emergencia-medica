﻿using System.Collections.Generic;

namespace EmergenciaMedica.Dominio.Entities
{
    public class UnidadeSaude
    {
        #region Propriedades
            public int Id { get; set; }
            public string TipoInstituicao { get; set; }
            public string TipoEmergencia { get; set; }
            public string Nome { get; set; }
            public string Endereco { get; set; }
            public string Telefone { get; set; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }
            public string LinkPt { get; set; }
            public string LinkEs { get; set; }
            public string EspecialidadesPt { get; set; }
            public string EspecialidadesEn { get; set; }
            public string EspecialidadesEs { get; set; }

        #endregion

    }
}
