﻿/*
    Models
*/
function UnidadeSaude(objeto) {
    var self = this;
    self.Id = objeto.Id;
    self.TipoInstituto = objeto.TipoInstituto;
    self.TipoEmergencia = objeto.TipoEmergencia;
    self.Nome = objeto.Nome;
    self.Endereco = objeto.Endereco;
    self.Telefone = objeto.Telefone;
    self.Latitude = objeto.Latitude;
    self.Longitude = objeto.Longitude;
    self.LinkPt = objeto.LinkPt;
    self.LinkEs = objeto.LinkEs;
    self.Distancia = ko.observable();
    self.DistanciaText = ko.observable();
}

function GridUnidadesSaudeModel() {
    var self = this;
    self.itens = ko.observableArray([]);
    self.Endereco = ko.observable();
    self.BuscarPeloEndereco = function () {
        buscaDistancias();
    }
    self.TracarRotaPeloEndereco = function (item) {
        tracarRotaPeloEndereco(item);
    }

    self.LimparRota = function () {
        limparRota();
    }
}

function MsgErroModel() {
    var self = this;
    self.MsgErro = ko.observable();
}

/*
    Registro Binds
*/

var vmGridUnidadesSaudeModel = new GridUnidadesSaudeModel();
ko.applyBindings(vmGridUnidadesSaudeModel, document.getElementById("grid_unidade_saude"));

var vmMsgErroModel = new MsgErroModel();
ko.applyBindings(vmMsgErroModel, document.getElementById("msg"));



/*
    Funções js 
*/

function buscarUnidadesSaude() {
        $.ajax({
            url: "/UnidadeSaude/BuscarUnidadesSaude",
            type: "GET",
            dataType: "json",
            contentType: "application/json",
            success: function (data) {
                var modelos = [];
                if (data.sucesso) {

                    data.dados.forEach(function (item) {
                        modelos.push(new UnidadeSaude(item));
                    });

                    vmGridUnidadesSaudeModel.itens(modelos);
                    criarMarker(vmGridUnidadesSaudeModel.itens());
                }
                else {
                    console.log('Ocorreu um erro: ' + data.erro);
                }
            }
        });
}

/*
    Maps
*/

var map;
var directionsDisplay;
var directionsService;


function initMap() {
    map = new google.maps.Map(document.getElementById('meuMapa'), {
        center: { lat: -29.990345, lng: -51.183028 },
        zoom: 10
    });
    directionsDisplay = new google.maps.DirectionsRenderer();
    directionsService = new google.maps.DirectionsService();
}

function buscaDistancias() {
    var origens = [];
    vmGridUnidadesSaudeModel.itens().forEach(function (item) {
        buscarDistanciaItem(item);
    });

}
function limparRota() {
    directionsDisplay.setMap(null);
}


function buscarDistanciaItem(item) {
    if (vmGridUnidadesSaudeModel.Endereco()) {
        var latLong = { lat: item.Latitude, lng: item.Longitude };

        var service = new google.maps.DistanceMatrixService();
        service.getDistanceMatrix(
          {
              origins: [latLong],
              destinations: [vmGridUnidadesSaudeModel.Endereco()],
              travelMode: 'DRIVING',
              unitSystem: google.maps.UnitSystem.METRIC,
              avoidHighways: false,
              avoidTolls: false,
          }, callback);

        function callback(response, status) {

            if (status !== 'OK') {
                console.log('Erro: ' + status);
            } else {
                if (response.rows.length) {
                    var element = response.rows[0].elements[0];
                    item.Distancia(element.distance.value);
                    item.DistanciaText(element.distance.text);
                    reordenarLista(item);
                }
            }

        }
    } else {
        showError("Digite um endereço!");
    }
}

function reordenarLista(item) {

    var unidades = vmGridUnidadesSaudeModel.itens();
    unidades.sort(function (a, b) {
        if (a.Distancia() > b.Distancia()) {
            return 1;
        }
        if (a.Distancia() < b.Distancia()) {
            return -1;
        }

        return 0;
    });

    vmGridUnidadesSaudeModel.itens(unidades);
}

function tracarRotaPeloEndereco(item) {

    if (vmGridUnidadesSaudeModel.Endereco()) {
        var latLong = { lat: item.Latitude, lng: item.Longitude };
        var request = {
            origin: latLong,
            destination: vmGridUnidadesSaudeModel.Endereco(),
            travelMode: google.maps.TravelMode.DRIVING
        };

        directionsDisplay.setMap(map);
        directionsService.route(request, function (result, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(result); 
            }
        });
    } else {
        console.log("Nenhum endereço digitado");
        showError("Digite um endereço!");
    }
}

function criarMarker(itens) {
    itens.forEach(function (item) {
        
        var latLong = { lat: item.Latitude, lng: item.Longitude };

        var contentString = '<div id="content">' +
            '<div id="siteNotice">' +
            '</div>' +
            '<h4>' + item.Nome + '</h4>'
        +'<div>'
            +'<ul>'
            + '<li><strong>Telefone</strong>: ' + item.Telefone + '</li>'
            + '<li><strong>Endereço</strong>: ' + item.Endereco + '</li>'
            +'</ul>'
        +'</div>';

        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });


        var marker = new google.maps.Marker({
            position: latLong,
            map: map,
            title: item.Nome
        });

        marker.addListener('click', function () {
            infowindow.open(map, marker);
        });

    });

}

function showError(erro) {
    vmMsgErroModel.MsgErro(erro);
    var x = document.getElementById("msg");
    x.className = "show";
    setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
}


$(document).ready(function () {
    buscarUnidadesSaude();
});