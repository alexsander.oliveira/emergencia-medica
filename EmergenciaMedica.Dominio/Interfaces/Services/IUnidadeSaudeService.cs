﻿using EmergenciaMedica.Dominio.Entities;

namespace EmergenciaMedica.Dominio.Interfaces.Services
{
    public interface IUnidadeSaudeService : IServiceBase<UnidadeSaude>
    {
    }
}
