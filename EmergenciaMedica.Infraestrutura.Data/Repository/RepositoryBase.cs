﻿
using EmergenciaMedica.Dominio.Interfaces.Repository;
using EmergenciaMedica.Infraestrutura.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EmergenciaMedica.Infraestrutura.Repository
{
    public class RepositoryBase<TEntity> : IDisposable, IRepositoryBase<TEntity> where TEntity : class
    {
        protected EmergenciaMedicaContext _context = new EmergenciaMedicaContext();

        public void Add(TEntity obj)
        {
            _context.Set<TEntity>().Add(obj);
            _context.SaveChanges();
        }

        public TEntity GetById(int id)
        {
            return _context.Set<TEntity>().Find(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _context.Set<TEntity>().ToList();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
