﻿using EmergenciaMedica.Aplicacao.Interfaces;
using EmergenciaMedica.Dominio.Interfaces.Services;
using System;
using System.Collections.Generic;

namespace EmergenciaMedica.Aplicacao.Implementation
{
    public class ApplicationServiceBase<TEntity> : IDisposable, IApplicationServiceBase<TEntity> where TEntity : class
    {
            
            private readonly IServiceBase<TEntity> _serviceBase;

            public ApplicationServiceBase(IServiceBase<TEntity> serviceBase)
            {
                _serviceBase = serviceBase;
            }

            public void Add(TEntity obj)
            {
                _serviceBase.Add(obj);
            }

            public TEntity GetById(int id)
            {
                return _serviceBase.GetById(id);
            }

            public IEnumerable<TEntity> GetAll()
            {
                return _serviceBase.GetAll();
            }

            public void Dispose()
            {
                _serviceBase.Dispose();
            }
    }
}
