﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EmergenciaMedica.MVC.Models;
using EmergenciaMedica.Dominio.Entities;
using AutoMapper;
using EmergenciaMedica.MVC.Mapper;

namespace EmergenciaMedica.MVC.AutoMapper
{
    public class AutoMapperConfig
    {
        public static IMapper Mapper { get; private set; }
        public static void RegisterMappings()
        {
            var _mapper = new MapperConfiguration((mapper) =>
            {
                mapper.AddProfile<DomainToModel>();
                mapper.AddProfile<ModelToDomain>();
            });

            Mapper = _mapper.CreateMapper();
        }
    }
}