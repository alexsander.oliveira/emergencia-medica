﻿using AutoMapper;
using EmergenciaMedica.Dominio.Entities;
using EmergenciaMedica.MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmergenciaMedica.MVC.Mapper
{
    public class DomainToModel : Profile
    {
        public DomainToModel()
        {
            CreateMap<UnidadeSaude, UnidadeSaudeVM>();
        }   
    }
}