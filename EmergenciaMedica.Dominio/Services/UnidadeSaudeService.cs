﻿using EmergenciaMedica.Dominio.Entities;
using EmergenciaMedica.Dominio.Interfaces.Repository;
using EmergenciaMedica.Dominio.Interfaces.Services;

namespace EmergenciaMedica.Dominio.Services
{
    public class UnidadeSaudeService : ServiceBase<UnidadeSaude>, IUnidadeSaudeService
    {
        private readonly IUnidadeSaudeRepository _unidadeSaudeRepository;

        public UnidadeSaudeService(IUnidadeSaudeRepository unidadeSaudeRepository)
            : base(unidadeSaudeRepository)
        {
            _unidadeSaudeRepository = unidadeSaudeRepository;
        }
    }
}
