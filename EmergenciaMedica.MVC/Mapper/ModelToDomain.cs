﻿using EmergenciaMedica.Dominio.Entities;
using EmergenciaMedica.MVC.Models;
using AutoMapper;

namespace EmergenciaMedica.MVC.Mapper
{
    public class ModelToDomain : Profile
    {
        public ModelToDomain()
        {
            CreateMap<UnidadeSaudeVM, UnidadeSaude>();
        }
    }
}