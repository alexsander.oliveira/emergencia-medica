# Emergência Médica
    Site que lista as Unidades de Saúde de Porto Alegre por ordem de proximidade de um endereço.

# Tecnologias
    .NET Framework 4.5.2
    AutoMapper
    Ninject
    EntityFramework
    Knockout 3.4.2
    Bootstrap
    Jquery
# Link da Publicação
Projeto publicado no <a href="http://emergencia-medica.azurewebsites.net" target="_blank">Microsoft Azure</a>

# Desenvolvedor:
Dsenvolvido por [Alexsander Oliveira]