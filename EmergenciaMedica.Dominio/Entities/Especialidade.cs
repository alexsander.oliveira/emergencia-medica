﻿

namespace EmergenciaMedica.Dominio.Entities
{
    public class Especialidade
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Idioma { get; set; }
        public int UnidadeSaudeId { get; set; }
        public virtual UnidadeSaude UnidadeSaude { get; set; }
    }
}
